OPTION DEFAULT INTEGER

DIM level(21) AS STRING LENGTH 40
level(0) =  "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"
level(1) =  "WO   ..D.D.D.W........r..r..r..rrrr....W"
level(2) =  "W....  DD ..DrD.D.....r..r..r..rD.r....W"
level(3) =  "W.. w..D...D.W..D.D...rrrr..r..rrrr....W"
level(4) =  "W..Dw...W.D..D.....r..r..r..r..r..r....W"
level(5) =  "W.D.wD.D...D.W...D.wwwwwwww............W"
level(6) =  "W...w.D.D.D....D...r....O...W...rrrrrr.W"
level(7) =  "WD..w...D..D.W..D..wwwwwwww.....rrrrrrrW"
level(8) =  "W...w.D..D.D.......rD.DO....W...rrrrrr.W"
level(9) =  "W...W.....D.... ...wwwwwwww.....rrrrrr.W"
level(10) = "W...W.......... ............W...rrrrrr.W"
level(11) = "W.OrWOr..f . r. .rrD........rr..rrrrrr.W"
level(12) = "W.ODW D.. ..OD. .rr.........rr..rrrrrr.W"
level(13) = "W.O.W.... ..... .wrr.......rrw....  ...W"
level(14) = "W.O.W.... ..... ..wrr.....rDw.....  W..W"
level(15) = "W.O.W.... ..... ...wrr   rrw......  ...W"
level(16) = "W.O.W.......    ....wr   rw.....    .W.W"
level(17) = "W.O.W................w   w......     ..W"
level(18) = "W.O.W............DD..     ..W...      .W"
level(19) = "W.         ..........     .....        W"
level(20) = "W..............................        W"
level(21) = "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW"



' TILE ENCODING (FIXED PROPERTIES):
'
'                        64      56      48      40      32      24      16       8       0
'                         /       /       /       /       /       /       /       /       /
CONST BLANK%          = &B0010000000000000000000000000000000000000000000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST ASCII%          = &B0000000000000000000000000000000000000000000000000000000011111111
'                         /       /       /       /       /       /       /       /       /
CONST SPRITE_PAGE%    = &B0000000000000000000000000000000011110000000000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST SPRITE_ROW%     = &B0000000000000000000000000000111100000000000000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST SPRITE_FRAME%   = &B0000000000000000000000001111000000000000000000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST HEAVY%          = &B0000000000000000000000010000000000000000000000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST ANIMATED%       = &B0000000000000000000000100000000000000000000000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST ROUNDED%        = &B0000000000000000000001000000000000000000000000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST EXPLODABLE%     = &B0000000000000000000010000000000000000000000000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST EDIBLE%         = &B0000000000000000000100000000000000000000000000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST INERT%          = &B0000000000000000001000000000000000000000000000000000000000000000


' TILE ENCODING (MUTABLE PROPERTIES):
'
'                        64      56      48      40      32      24      16       8       0
'                         /       /       /       /       /       /       /       /       /
CONST FALLING%        = &B0000000000000000000000000000000000000000000000100000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST EXPLODING%      = &B0000000000000000000000000000000000000000000000000111000000000000
'                         /       /       /       /       /       /       /       /       /
CONST DIRECTION%      = &B0000000000000000000000000000000000000000111100000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST LEFT%           = &B0000000000000000000000000000000000000000000100000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST RIGHT%          = &B0000000000000000000000000000000000000000001000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST UP%             = &B0000000000000000000000000000000000000000010000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST DOWN%           = &B0000000000000000000000000000000000000000100000000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST FIRE_DOWN%      = &B0000000000000000000000000000000000000000000010000000000000000000
'                         /       /       /       /       /       /       /       /       /
CONST CAPSIKIN_FLAG%  = &B0000000000000000000000000000000000000000000001000000000000000000
                                                                         
                                                                                         
'COMMANDS BITMASKS                                                                        
'                                                                                         
'                        64      56      48      40      32      24      16       8       0
'                         /       /       /       /       /       /       /       /       /
CONST CMD_ROW%        = &B0000000011111111110000000000000000000000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CMD_COL%        = &B0000000000000000001111111111000000000000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_TYPE_MASK%   = &B0000000000000000000000000000111111111111000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_STAND_FIREFLY%=&B0000000000000000000000000000010000000000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_MOVE_FIREFLY%= &B0000000000000000000000000000001000000000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_MOVE_HEAVY%  = &B0000000000000000000000000000000100000000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_SCAN%        = &B0000000000000000000000000000000010000000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_SET_BITS%    = &B0000000000000000000000000000000001000000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_CLEAR_BITS%  = &B0000000000000000000000000000000000100000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_MOVE_RCKFD%  = &B0000000000000000000000000000000000010000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_PUSH_RCKFD%  = &B0000000000000000000000000000000000001000000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_STAND_RCKFD% = &B0000000000000000000000000000000000000100000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_TILE_SUBST%  = &B0000000000000000000000000000000000000010000000000000000000000000 
'                         /       /       /       /       /       /       /       /       /
CONST CT_EXPLODE%     = &B0000000000000000000000000000000000000001000000000000000000000000 





'                         /       /       /       /       /       /       /       /       /
CONST CT_BITS_MASK%   = &B0000000000000000000000000000000000000000111111111111111100000000 


CONST CT_BITS_MASK_INV% = INV CT_BITS_MASK%
CONST CT_TYPE_MASK_INV% = INV CT_TYPE_MASK%
CONST CMD_ROW_INV% = INV CMD_ROW%
CONST CMD_COL_INV% = INV CMD_COL%


FUNCTION GETCMDROW( cmd )
  GETCMDROW = ((cmd AND CMD_ROW%) >> 46)
END FUNCTION

FUNCTION SETCMDROW( cmd, row )
  SETCMDROW = (cmd AND CMD_ROW_INV%) OR (CMD_ROW% AND (row << 46))
END FUNCTION


FUNCTION GETCMDCOL( cmd )
  GETCMDCOL = ((cmd AND CMD_COL%) >> 36)
END FUNCTION

FUNCTION SETCMDCOL( cmd AS INTEGER, col AS INTEGER ) AS INTEGER
  SETCMDCOL = (cmd AND CMD_COL_INV%) OR (CMD_COL% AND (col << 36))
END FUNCTION


FUNCTION GETCMDTYPE( cmd AS INTEGER ) AS INTEGER
  GETCMDTYPE = cmd AND CT_TYPE_MASK%
END FUNCTION

FUNCTION SETCMDTYPE( cmd AS INTEGER, type AS INTEGER ) AS INTEGER
  SETCMDTYPE = (cmd AND CT_TYPE_MASK_INV%) OR (CT_TYPE_MASK% AND type)
END FUNCTION


FUNCTION GETCMDBITS( cmd AS INTEGER ) AS INTEGER
  GETCMDBITS = cmd AND CT_BITS_MASK%
END FUNCTION

FUNCTION SETCMDBITS( cmd AS INTEGER, bits AS INTEGER ) AS INTEGER
  SETCMDBITS = (cmd AND CT_BITS_MASK_INV%) OR (CT_BITS_MASK% AND bits)
END FUNCTION



SUB MAKECMD col, row, type, bits, cmdList(), index
  cmdList(index) = SETCMDBITS(SETCMDTYPE(SETCMDCOL(SETCMDROW(0,row),col),type),bits)
  index = index + 1
END SUB

SUB MAKESCANCMD col, row, scanList(), index
  scanList(index) = (row << 46) OR (col << 36)
  index = index + 1
END SUB



FUNCTION GETBDF( tile AS INTEGER ) AS STRING
  GETBDF = CHR$(tile AND ASCII%)
END FUNCTION

FUNCTION SETBDF( tile AS INTEGER, bdf AS STRING ) AS INTEGER
  SETBDF = ((tile AND INV ASCII%) OR (ASC(bdf) AND ASCII%))
END FUNCTION

FUNCTION GETSPRITEPAGE( tile AS INTEGER ) AS INTEGER
  GETSPRITEPAGE = ((tile AND SPRITE_PAGE%) >> 28)
END FUNCTION

FUNCTION SETSPRITEPAGE( tile AS INTEGER, spage AS INTEGER ) AS INTEGER
  SETSPRITEPAGE = (TILE AND INV SPRITE_PAGE%) OR (SPRITE_PAGE% AND (spage << 28))
END FUNCTION


FUNCTION GETSPRITEROW( tile AS INTEGER ) AS INTEGER
  GETSPRITEROW = ((tile AND SPRITE_ROW%) >> 32)
END FUNCTION


FUNCTION SETSPRITEROW( tile AS INTEGER, row AS INTEGER ) AS INTEGER
  SETSPRITEROW = (TILE AND INV SPRITE_ROW%) OR (SPRITE_ROW% AND (row << 32))
END FUNCTION


FUNCTION GETSPRITEFRAMES( tile AS INTEGER ) AS INTEGER
  GETSPRITEFRAMES = ((tile AND SPRITE_FRAME%) >> 36)
END FUNCTION

FUNCTION SETSPRITEFRAMES( tile AS INTEGER, frames AS INTEGER ) AS INTEGER
  SETSPRITEFRAMES = (TILE AND INV SPRITE_FRAME%) OR (SPRITE_FRAME% AND (frames << 36))
END FUNCTION


FUNCTION GETROUNDED( tile AS INTEGER ) AS INTEGER
  GETROUNDED = tile AND ROUNDED%
END FUNCTION

FUNCTION SETROUNDED( tile AS INTEGER ) AS INTEGER
  SETROUNDED = tile OR ROUNDED%
END FUNCTION


FUNCTION GETANIMATED( tile AS INTEGER ) AS INTEGER
  GETANIMATED = tile AND ANIMATED%
END FUNCTION

FUNCTION SETANIMATED( tile AS INTEGER ) AS INTEGER
  SETANIMATED = tile OR ANIMATED%
END FUNCTION


FUNCTION GETHEAVY( tile AS INTEGER ) AS INTEGER
  GETHEAVY = tile AND HEAVY%
END FUNCTION

FUNCTION SETHEAVY( tile AS INTEGER ) AS INTEGER
  SETHEAVY = tile OR HEAVY%
END FUNCTION


FUNCTION GETEDIBLE( tile AS INTEGER ) AS INTEGER
  GETEDIBLE = tile AND EDIBLE%
END FUNCTION

FUNCTION SETEDIBLE( tile AS INTEGER ) AS INTEGER
  SETEDIBLE = tile OR EDIBLE%
END FUNCTION


FUNCTION GETDIR( tile AS INTEGER ) AS INTEGER
  GETDIR = tile AND DIRECTION%
END FUNCTION 

FUNCTION SETDIR( tile AS INTEGER, dir AS INTEGER ) AS INTEGER
  SETDIR = ((tile AND (INV DIRECTION%)) OR dir)
END FUNCTION


FUNCTION GETCAPSIKIN( tile AS INTEGER ) AS INTEGER
  GETCAPSIKIN = tile AND CAPSIKIN_FLAG%
END FUNCTION

FUNCTION SETCAPSIKIN( tile AS INTEGER ) AS INTEGER
  SETCAPSIKIN = (tile OR CAPSIKIN_FLAG%)
END FUNCTION

FUNCTION CLEARCAPSIKIN( tile AS INTEGER ) AS INTEGER
  CLEARCAPSIKIN = tile AND INV CAPSIKIN_FLAG%
END FUNCTION


FUNCTION GETINERT( tile AS INTEGER ) AS INTEGER
  GETINERT = tile AND INERT%
END FUNCTION

FUNCTION SETINERT( tile AS INTEGER ) AS INTEGER
  SETINERT = (tile OR INERT%)
END FUNCTION


FUNCTION GETEXPLODABLE( tile AS INTEGER ) AS INTEGER
  GETEXPLODABLE = tile AND EXPLODABLE%
END FUNCTION

FUNCTION SETEXPLODABLE( tile AS INTEGER ) AS INTEGER
  SETEXPLODABLE = (tile OR EXPLODABLE%)
END FUNCTION


FUNCTION GETFALLING( tile AS INTEGER ) AS INTEGER
  GETFALLING = tile AND FALLING%
END FUNCTION

FUNCTION SETFALLING( tile AS INTEGER ) AS INTEGER
  SETFALLING = tile OR FALLING%
END FUNCTION

FUNCTION CLEARFALLING( tile AS INTEGER ) AS INTEGER
  CLEARFALLING = tile AND INV FALLING%
END FUNCTION


FUNCTION GETEXPLODING( tile AS INTEGER ) AS INTEGER
  GETEXPLODING = ((tile AND EXPLODING%) >> 12)
END FUNCTION

FUNCTION SETEXPLODING( tile AS INTEGER, kaboomStage AS INTEGER ) AS INTEGER
  SETEXPLODING = (tile AND INV EXPLODING%) OR (EXPLODING% AND (kaboomStage << 12))
END FUNCTION





'Converts data from an array that represents a room by one string per row to a 2d numeric array
'
'w - width of each string line
'h - the number of lines in bdfData$
'bdfData - array of strings where each line represents one row
'board - target 2d array that represents the board
SUB BDF2BOARD w, h, bdfData() AS STRING, board() AS INTEGER
  LOCAL textLine$, symbol$, n, m
  FOR n = 0 TO h-1
    FOR m = 0 TO w-1
      symbol$ = MID$(bdfData(n),m+1,1)
      board(m,n) = BDF2TILE(symbol$)
    NEXT m
  NEXT n
END SUB


'Pretty prints a 2d array starting from its top left corner
'
'w - width of the printed output
'h - height of the printed output
'array - array to print
'ascii - show content as ascii if non zero
SUB PPRINT2D w, h, array() AS INTEGER, ascii
  LOCAL encoded AS STRING
  LOCAL n AS INTEGER
  LOCAL m AS INTEGER
  FOR n = 0 TO h-1
    FOR m = 0 TO w-1
      encoded = GETBDF(array(m,n))
      IF ascii = 0 THEN PRINT encoded; " "; ELSE PRINT CHR$(encoded); " ";
    NEXT m
    PRINT
  NEXT n
END SUB

FUNCTION SETSPRITEINFO(tile AS INTEGER, spage AS INTEGER, row AS INTEGER, frameCount AS INTEGER )
  SETSPRITEINFO = SETSPRITEFRAMES(SETSPRITEROW(SETSPRITEPAGE(tile,spage),row),frameCount)
END FUNCTION

SUB LOADSPRITESFOR bdf AS STRING, name AS STRING, resolution AS INTEGER, pgOverride AS INTEGER
  LOCAL layer = 1
  LOCAL tile = BDF2TILE(bdf)
  LOCAL row = GETSPRITEROW(tile)
  LOCAL spage
  IF pgOverride >= 0 THEN 
    spage = pgOverride 
  ELSE 
    spage = GETSPRITEPAGE(tile)
  ENDIF
  PAGE WRITE spage
  SPRITE LOAD name + STR$(resolution) + ".spr",1
  LOCAL n
  FOR n = 1 TO GETSPRITEFRAMES(tile)
    BOX (n-1)*resolution,(row-1)*resolution,resolution,resolution,1,RGB(black),RGB(black)
    SPRITE SHOW n,(n-1)*resolution,(row-1)*resolution,layer
  NEXT n
  SPRITE CLOSE ALL
END SUB

SUB LOADALLSPRITES16 pgOverride AS INTEGER
  LOADSPRITESFOR "W", "steelwall",16, pgOverride
  LOADSPRITESFOR "w", "wall",16, pgOverride
  LOADSPRITESFOR ".", "dirt",16, pgOverride
  LOADSPRITESFOR " ", "emptyspace",16, pgOverride
  LOADSPRITESFOR "D", "diamond",16, pgOverride
  LOADSPRITESFOR "r", "rock",16, pgOverride
  LOADSPRITESFOR "f", "rockford",16, pgOverride
  LOADSPRITESFOR "g", "rrockford",16, pgOverride
  LOADSPRITESFOR "h", "lrockford",16, pgOverride
  LOADSPRITESFOR "!", "s1explosion",16,pgOverride
  LOADSPRITESFOR "@", "s2explosion",16,pgOverride
  LOADSPRITESFOR "#", "s3explosion",16,pgOverride
  LOADSPRITESFOR "$", "s4explosion",16,pgOverride
  LOADSPRITESFOR "O", "firefly",16,pgOverride 
END SUB

FUNCTION BDF2TILE( bdf AS STRING ) AS INTEGER
  LOCAL tile = SETBDF(0,bdf)
  IF bdf = "W" THEN 
    BDF2TILE = SETINERT(SETSPRITEINFO(tile,3,1,1))
  ELSEIF bdf = "w" THEN 
    BDF2TILE = SETROUNDED(SETSPRITEINFO(tile,3,2,1))
  ELSEIF bdf = "." THEN 
    BDF2TILE = SETINERT(SETEDIBLE(SETSPRITEINFO(tile,3,3,1)))
  ELSEIF bdf = " " THEN 
    BDF2TILE = SETEDIBLE(SETSPRITEINFO(tile,3,4,1))
  ELSEIF bdf = "D" THEN 
    BDF2TILE = SETANIMATED(SETEDIBLE(SETHEAVY(SETROUNDED(SETSPRITEINFO(tile,3,5,15)))))
  ELSEIF bdf = "r" THEN 
    BDF2TILE = SETHEAVY(SETROUNDED(SETSPRITEINFO(tile,3,6,1)))
  ELSEIF bdf = "!" THEN
    BDF2TILE = SETSPRITEINFO(tile,3,7,1)
  ELSEIF bdf = "@" THEN
    BDF2TILE = SETSPRITEINFO(tile,3,8,1)
  ELSEIF bdf = "#" THEN
    BDF2TILE = SETSPRITEINFO(tile,3,9,1)
  ELSEIF bdf = "$" THEN
    BDF2TILE = SETSPRITEINFO(tile,3,10,1)
  ELSEIF bdf = "f" THEN 
    BDF2TILE = SETEXPLODABLE(SETANIMATED(SETSPRITEINFO(tile,4,1,4)))
  ELSEIF bdf = "g" THEN 
    BDF2TILE = SETBDF(SETEXPLODABLE(SETANIMATED(SETSPRITEINFO(tile,4,2,4))),"f")
  ELSEIF bdf = "h" THEN 
    BDF2TILE = SETBDF(SETEXPLODABLE(SETANIMATED(SETSPRITEINFO(tile,4,3,4))),"f")
  ELSEIF bdf = "O" THEN
    BDF2TILE = SETDIR(SETBDF(SETEXPLODABLE(SETANIMATED(SETSPRITEINFO(tile,4,4,6))),"O"),LEFT%)
  ENDIF

END FUNCTION

DIM STWALL AS INTEGER = BDF2TILE("W")
DIM STWALL_BASE AS INTEGER = STWALL AND ASCII%
DIM WALL AS INTEGER = BDF2TILE("w")
DIM WALL_BASE AS INTEGER = WALL AND ASCII%
DIM DIRT AS INTEGER = BDF2TILE(".")
DIM DIRT_BASE AS INTEGER = DIRT AND ASCII%
DIM EMPTY AS INTEGER = BDF2TILE(" ")
DIM EMPTY_BASE AS INTEGER = EMPTY AND ASCII%
DIM DIAMOND AS INTEGER = BDF2TILE("D")
DIM DIAMOND_BASE AS INTEGER = DIAMOND AND ASCII%
DIM ROCK AS INTEGER = BDF2TILE("r")
DIM ROCK_BASE AS INTEGER = ROCK AND ASCII%
DIM ROCKFORD AS INTEGER = BDF2TILE("f")
DIM ROCKFORD_BASE AS INTEGER = ROCKFORD AND ASCII%
DIM RROCKFORD AS INTEGER = BDF2TILE("g")
DIM RROCKFORD_BASE AS INTEGER = RROCKFORD AND ASCII%
DIM LROCKFORD AS INTEGER = BDF2TILE("h")
DIM LROCKFORD_BASE AS INTEGER = LROCKFORD AND ASCII%
DIM S1_EXPLOSION AS INTEGER = BDF2TILE("!")
DIM S1_EXPLOSION_BASE AS INTEGER = S1_EXPLOSION AND ASCII%
DIM S2_EXPLOSION AS INTEGER = BDF2TILE("@")
DIM S2_EXPLOSION_BASE AS INTEGER = S2_EXPLOSION AND ASCII%
DIM S3_EXPLOSION AS INTEGER = BDF2TILE("#")
DIM S3_EXPLOSION_BASE AS INTEGER = S3_EXPLOSION AND ASCII%
DIM S4_EXPLOSION AS INTEGER = BDF2TILE("$")
DIM S4_EXPLOSION_BASE AS INTEGER = S4_EXPLOSION AND ASCII%
DIM FIREFLY AS INTEGER = BDF2TILE("O")
DIM FIREFLY_BASE AS INTEGER = FIREFLY AND ASCII%

DIM EXPLOSIONS(5) AS INTEGER = (0,S1_EXPLOSION,S2_EXPLOSION,S3_EXPLOSION,S4_EXPLOSION,EMPTY)

FUNCTION FINDROCKFORDCOL(room() AS INTEGER, roomWidth as INTEGER, roomHeight AS INTEGER) AS INTEGER
  LOCAL n,m
  FOR n = 0 TO roomHeight-1
    FOR m = 0 TO roomWidth-1
      IF (room(m,n) AND ASCII%) = ROCKFORD_BASE THEN FINDROCKFORDCOL = m
    NEXT
  NEXT
END FUNCTION

FUNCTION FINDROCKFORDROW(room() AS INTEGER, roomWidth as INTEGER, roomHeight AS INTEGER) AS INTEGER
  LOCAL n,m
  FOR n = 0 TO roomHeight-1
    FOR m = 0 TO roomWidth-1
      IF (room(m,n) AND ASCII%) = ROCKFORD_BASE THEN FINDROCKFORDROW = n
    NEXT
  NEXT
END FUNCTION

SUB MUT room() AS INTEGER, col as INTEGER, row AS INTEGER, replacement AS INTEGER
  LOCAL tile AS INTEGER = room(col,row)
  IF (tile AND ASCII%) <> STWALL_BASE THEN room(col,row)=replacement
END SUB

FUNCTION LEFTOF(dir AS INTEGER)
  SELECT CASE dir
    CASE LEFT%
      LEFTOF = DOWN%
    CASE RIGHT%
      LEFTOF = UP%
    CASE UP%
      LEFTOF = LEFT%
    CASE DOWN%
      LEFTOF = RIGHT%
  END SELECT
END FUNCTION

FUNCTION RIGHTOF(dir AS INTEGER)
  SELECT CASE dir
    CASE LEFT%
      RIGHTOF = UP%
    CASE RIGHT%
      RIGHTOF = DOWN%
    CASE UP%
      RIGHTOF = RIGHT%
    CASE DOWN%
      RIGHTOF = LEFT%
  END SELECT
END FUNCTION

