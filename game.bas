OPTION EXPLICIT
OPTION DEFAULT INTEGER
FONT 7

#INCLUDE "utils.inc" 

'#INCLUDE "utils-rock-path.inc" 

'#INCLUDE "utils-clear.inc" 

'#INCLUDE "utilstest.inc" 

DIM processedScans = 0
DIM processedCmds = 0
DIM rawScans = 0
DIM rawCmds = 0


DIM animClock = 0
DIM renderClock = 0
DIM gameSemaphore = 0
DIM gameClock = 0
DIM caveClock = 100

CONST TILE_SIZE=16
CONST COORDS_MASK = CMD_COL% OR CMD_ROW%

DIM slidingViewport = 1

IF slidingViewport = 1 THEN
  CONST SLIDE_DIVISOR=4
  CONST LEFT_EDGE_BUFFER = 2
  CONST RIGHT_EDGE_BUFFER = 3
  CONST TOP_EDGE_BUFFER = 2
  CONST BOTTOM_EDGE_BUFFER = 2
ELSE
  CONST SLIDE_DIVISOR=1
  CONST LEFT_EDGE_BUFFER = 5
  CONST RIGHT_EDGE_BUFFER = 5
  CONST TOP_EDGE_BUFFER = 3
  CONST BOTTOM_EDGE_BUFFER = 3
ENDIF

CONST SCREEN_PIXELS_W = MM.HRES
CONST SCREEN_PIXELS_H = MM.VRES

DIM VIEWPORT_W = 20
DIM VIEWPORT_H = 11

DIM VIEWPORT_W_PX = VIEWPORT_W * TILE_SIZE
DIM VIEWPORT_HALF_W_PX = VIEWPORT_W_PX / 2
DIM VIEWPORT_H_PX = VIEWPORT_H * TILE_SIZE
DIM VIEWPORT_HALF_H_PX = VIEWPORT_H_PX / 2

DIM viewportX = 0
DIM viewportY = 0
DIM targetViewportX = viewportX
DIM targetViewportY = viewportY

DIM ROOM_W = 40
DIM ROOM_H = 22

DIM ROOM_W_PX = ROOM_W * TILE_SIZE
DIM ROOM_H_PX = ROOM_H * TILE_SIZE

'declare the room data structure
DIM room(ROOM_W,ROOM_H)

DIM keyPress  = 0
DIM firePress = 0
DIM rockfordCol = 0
DIM rockfordRow = 0
DIM rockfordTile = RROCKFORD
DIM rockfordToDraw = ROCKFORD
DIM gameTickTime = 0
DIM magicWallStatus = 0 '0 - inactive, 1 - active, -1 - expired
DIM magicWallActiveTime = 0
DIM magicWallTimeLimit = 20

MODE 3, 8, 0, renderTick

SETTICK 130,animTick,1
SETTICK 130,gameTick,2
SETTICK 1000,caveTick,3

LOADALLSPRITES16 -1

PAGE WRITE 1

'''''''''''GAME LOOP'''''''''''''''

CAVESTART:

CLS

RESETLEVEL

CAVELOOP ROOM_H*ROOM_W

'''''''''''GAME LOGIC''''''''''''''

SUB CAVELOOP roomSize AS INTEGER
  LOCAL scanList(roomSize) AS INTEGER
  LOCAL cmdList(roomSize) AS INTEGER
  LOCAL scanListIndex = 0
  LOCAL cmdIndex = 0
  
  MATH SET BLANK%,cmdList() 
  MATH SET BLANK%,scanList()
 
  FILLSCANLIST scanList(), scanListIndex
  
  DO
    KEYSCAN
    RENDERSCREEN 1
    IF magicWallActiveTime > magicWallTimeLimit AND magicWallStatus = 1 THEN TURNMGWALLOFF
    IF gameSemaphore = 1 THEN
      gameSemaphore = 0
      RENDERSCREEN 0
      KEYSCAN
      processedScans = 0
      processedCmds = 0
      rawScans = 0
      rawCmds = 0  
      rockfordCol = 0 : rockfordRow = 0
      TIMER=0
      MATH SET BLANK%,cmdList()    
      cmdIndex = 0
      PROCESSTICK cmdList(), scanList(), scanListIndex, cmdIndex, roomSize
      gameClock = gameClock + 1
      keyPress = 0
      firePress = 0
      gameTickTime = TIMER
    ENDIF
  LOOP
END SUB


SUB PROCESSTICK cmdList(), scanList(), scanListIndex, cmdIndex, size
  LOCAL sc,cmd,pn,m,col,row,tile,basis,below,testCol,testRow,dir,prefDir,finalDir
  LOCAL stage,nextStage,cmdType,adjTile,offset,isButter
  LOCAL lastScan=0
  LOCAL lastCmd=0
  PAGE WRITE 0
  SORT scanList()
  FOR pn = 0 TO size-1
    sc = scanList(pn)
    IF sc = BLANK% THEN 
      EXIT FOR
    ELSEIF sc <> lastScan THEN
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      '''''''''''''''''''' SCANNNING ROUTINE ''''''''''''''''''''''''''''''
      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      col = (sc AND CMD_COL%) >> 36
      row = (sc AND CMD_ROW%) >> 46
      tile = room(col,row)
      basis = tile AND 255
      SELECT CASE basis
        CASE EMPTY_BASE, DIRT_BASE, STWALL_BASE, WALL_BASE
          'nothing     
        CASE DIAMOND_BASE, ROCK_BASE
          below=room(col,row+1)
          IF (below AND 255)=EMPTY_BASE THEN
            MAKECMD col,row,CT_MOVE_HEAVY%,DOWN% OR FALLING%,cmdList(),cmdIndex
          ELSE
            IF (below AND ROUNDED%) <> 0 AND (FALLING% AND below) = 0 THEN
              IF (room(col-1,row) AND 255)=EMPTY_BASE AND (room(col-1,row+1) AND 255)=EMPTY_BASE THEN
                MAKECMD col,row,CT_MOVE_HEAVY%,LEFT% OR FALLING%,cmdList(),cmdIndex
              ELSEIF (room(col+1,row) AND 255)=EMPTY_BASE AND (room(col+1,row+1) AND 255)=EMPTY_BASE THEN
                MAKECMD col,row,CT_MOVE_HEAVY%,RIGHT% OR FALLING%,cmdList(),cmdIndex
              ELSE
                IF tile AND FALLING% THEN
                  MAKECMD col,row,CT_CLEAR_BITS%,FALLING%,cmdList(),cmdIndex
                ENDIF
                room(col-1,  row)=room(col-1,  row) OR CAPSIKIN_FLAG%
                room(col+1,  row)=room(col+1,  row) OR CAPSIKIN_FLAG%
                room(col-1,row+1)=room(col-1,row+1) OR CAPSIKIN_FLAG%
                room(col+1,row+1)=room(col+1,row+1) OR CAPSIKIN_FLAG%
                room(col,  row+1)=room(col,  row+1) OR CAPSIKIN_FLAG%
              ENDIF
            ELSEIF FALLING% AND tile THEN
              IF (below AND EXPLODABLE%) <> 0 THEN
                MAKEEXPLOSIONCMDS col,row+1,below AND YIELD_DIAMONDS%,cmdList(),cmdIndex
                play tone 5000,5000,50
              ELSEIF (below AND 255) = MAGIC_WALL_ON_BASE THEN
                MAKECMD col,row,CT_MG_CONVERT%,0,cmdList(),cmdIndex
              ELSEIF (below AND 255) = MAGIC_WALL_OFF_BASE AND magicWallStatus = 0 THEN
                TURNMGWALLON
                MAKECMD col,row,CT_MG_CONVERT%,0,cmdList(),cmdIndex
              ELSEIF (below AND 255) = MAGIC_WALL_OFF_BASE AND magicWallStatus = -1 THEN
                MAKECMD col,row,CT_MG_SWALLOW%,0,cmdList(),cmdIndex
              ELSE
                MAKECMD col,row,CT_CLEAR_BITS%,FALLING%,cmdList(),cmdIndex
              ENDIF
              room(col,row+1)=room(col,row+1) OR CAPSIKIN_FLAG%
          ENDIF
          ENDIF
        CASE FIREFLY_BASE, BUTTERFLY_BASE
          isButter = (tile AND 255) = BUTTERFLY_BASE
          dir = tile AND DIRECTION%
          prefDir = GETPREFDIR(isButter,dir)
          testCol = col
          testRow = row
          IF (room(col-1,row) AND 255)=ROCKFORD_BASE OR (room(col+1,row) AND 255)=ROCKFORD_BASE OR (room(col,row-1) AND 255)=ROCKFORD_BASE OR (room(col,row+1) AND 255)=ROCKFORD_BASE THEN
            MAKEEXPLOSIONCMDS col,row,tile AND YIELD_DIAMONDS%,cmdList(),cmdIndex
          ELSE
            DIR2OFFSETS prefDir,testCol,testRow
            IF (room(testCol,testRow) AND 255)=EMPTY_BASE THEN
              MAKECMD col,row,CT_MOVE_FLY%,prefDir,cmdList(),cmdIndex
            ELSE
              testCol = col
              testRow = row
              DIR2OFFSETS dir,testCol,testRow
              IF (room(testCol,testRow) AND 255)=EMPTY_BASE THEN
                MAKECMD col,row,CT_MOVE_FLY%,dir,cmdList(),cmdIndex
              ELSE
                finalDir = GETFINALDIR(isButter,dir)
                MAKECMD col,row,CT_STAND_FLY%,finalDir,cmdList(),cmdIndex
              ENDIF
            ENDIF
          ENDIF
        CASE ROCKFORD_BASE
          SCANROCKFORD tile, col, row, cmdList(), cmdIndex
        CASE S1_EXPLOSION_BASE, S2_EXPLOSION_BASE, S3_EXPLOSION_BASE, S4_EXPLOSION_BASE
          MAKECMD col,row,CT_EXPLODE%,0,cmdList(),cmdIndex
        CASE S1_EXPLOSIOND_BASE, S2_EXPLOSIOND_BASE, S3_EXPLOSIOND_BASE, S4_EXPLOSIOND_BASE
          MAKECMD col,row,CT_EXPLODE%,YIELD_DIAMONDS%,cmdList(),cmdIndex
      END SELECT
      processedScans = processedScans + 1
    ENDIF
    rawScans = rawScans + 1
    lastScan=sc
  NEXT pn

  MATH SET BLANK%,scanList()
  scanListIndex=0
  IF cmdIndex > 0 THEN SORT cmdList()
  LOCAL cmdCoords
  FOR m = 0 TO cmdIndex-1
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''''''''''''''''''''' COMMAND PROCESSING ROUTINE '''''''''''''''''''''''''''''''''
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    cmd = cmdList(m)
    cmdCoords = cmd AND COORDS_MASK
    IF cmd = BLANK% THEN 
      EXIT FOR
    ELSEIF cmdCoords <> lastCmd THEN
      col = (cmd AND CMD_COL%) >> 36
      row = (cmd AND CMD_ROW%) >> 46
      testCol = col
      testRow = row
      tile = room(col,row)
      cmdType = cmd AND CT_TYPE_MASK%

      SELECT CASE cmdType
        CASE CT_SET_BITS%
          room(col,row) = tile OR GETCMDBITS(cmd)
        CASE CT_CLEAR_BITS%
          room(col,row) = tile AND (INV (cmd AND CT_BITS_MASK%))
        CASE CT_MOVE_HEAVY%
          dir = cmd AND DIRECTION%
          DIR2OFFSETS dir,testCol,testRow
          IF (room(testCol,testRow) AND 255)=EMPTY_BASE THEN
            IF (CAPSIKIN_FLAG% AND tile) = 0 THEN
              MAKESCANCMD testCol,testRow,scanList(),scanListIndex
            ELSE
              ADDSCANFORHEAVY col,row,dir,scanList(),scanListIndex
            ENDIF
            room(col,row)=EMPTY
            room(testCol,testRow)=CLEARCAPSIKIN(tile) OR GETCMDBITS(cmd)
          ELSE
            MAKESCANCMD col,row,scanList(),scanListIndex
          ENDIF
        CASE CT_MOVE_FLY%
          dir = cmd AND DIRECTION%
          DIR2OFFSETS dir,testCol,testRow
          IF (room(testCol,testRow) AND 255)=EMPTY_BASE AND (room(col,row) AND 255)<>EMPTY_BASE THEN
            room(testCol,testRow)=CLEARCAPSIKIN(SETDIR(tile, dir))
            room(col,row)=EMPTY
            IF (CAPSIKIN_FLAG% AND tile) = 0 THEN
              MAKESCANCMD testCol,testRow,scanList(),scanListIndex
            ELSE
              ADDSCANFORHEAVY col,row,dir,scanList(),scanListIndex
            ENDIF
          ELSE
            MAKESCANCMD col,row,scanList(),scanListIndex
          ENDIF
        CASE CT_STAND_FLY%
          room(col,row)=SETDIR(tile, cmd AND DIRECTION%)
          MAKESCANCMD col,row,scanList(),scanListIndex
        CASE CT_EXPLODE%
          stage = GETEXPLODING(tile)
          nextStage = (stage+1) MOD 5
          IF cmd AND YIELD_DIAMONDS% THEN
            MUT room(), col,row, SETEXPLODING(EXPLOSIONSD(stage+1),nextStage)
          ELSE
            MUT room(), col,row, SETEXPLODING(EXPLOSIONS(stage+1),nextStage)
          ENDIF
          IF nextStage = 0 THEN ADDSCANAROUND1 col,row,scanList(),scanListIndex
          MAKESCANCMD col,row,scanList(),scanListIndex
        CASE CT_MG_CONVERT%
          room(col,row)=EMPTY
          MAKESCANCMD col,row-1,scanList(),scanListIndex
          MAKESCANCMD col-1,row-1,scanList(),scanListIndex
          MAKESCANCMD col+1,row-1,scanList(),scanListIndex
          IF room(col,row+2) = EMPTY THEN
            IF (tile AND ASCII%) = ROCK_BASE THEN
              room(col,row+2) = SETFALLING(DIAMOND)
            ELSE
              room(col,row+2) = SETFALLING(ROCK)
            ENDIF
            MAKESCANCMD col,row+2,scanList(),scanListIndex
          ENDIF
        CASE CT_MG_SWALLOW%
          room(col,row)=EMPTY
          MAKESCANCMD col,row-1,scanList(),scanListIndex
          MAKESCANCMD col-1,row-1,scanList(),scanListIndex
          MAKESCANCMD col+1,row-1,scanList(),scanListIndex
        CASE CT_MOVE_RCKFD% 
          IF (tile AND ASCII%) = ROCKFORD_BASE THEN
            rockfordCol = col
            rockfordRow = row
            dir = cmd AND DIRECTION%      
            DIR2OFFSETS dir,testCol,testRow
            adjTile = room(testCol,testRow)
            IF adjTile AND EDIBLE% THEN
              ADDSCANAROUNDROCKFORD col,row,dir,scanList(), scanListIndex
              SELECT CASE (adjTile AND ASCII%)
                CASE DIAMOND_BASE
                  PLAY TONE 600,600,30
                CASE DIRT_BASE
                  PLAY TONE 300,300,30
                CASE EMPTY_BASE
                  PLAY TONE 200,200,30
              END SELECT
              IF cmd AND FIRE_DOWN% THEN
                room(testCol,testRow)=EMPTY
                room(col,row)=rockfordToDraw
                rockfordCol = col
                rockfordRow = row
              ELSE
                room(col,row)=EMPTY
                room(testCol,testRow)=rockfordToDraw
                rockfordCol = testCol
                rockfordRow = testRow
              ENDIF
              POSITIONVIEWPORT rockfordCol,rockfordRow
            ENDIF
            MAKESCANCMD rockfordCol,rockfordRow, scanList(), scanListIndex
          ENDIF
        CASE CT_PUSH_RCKFD%
          IF (tile AND ASCII%) = ROCKFORD_BASE THEN
            rockfordCol = col
            rockfordRow = row
            dir = cmd AND DIRECTION%
            offset = -1
            IF dir = RIGHT% THEN 
              offset = 1
            ENDIF
            IF (room(col+(2*offset),row) AND 255)=EMPTY_BASE AND (room(col+offset,row+1) AND 255)<>EMPTY_BASE THEN
              PLAY TONE 900,900,30
              IF cmd AND FIRE_DOWN% THEN
                room(col,row)=rockfordToDraw
                room(col+(2*offset),row)=room(col+offset,row)
                room(col+offset,row)=EMPTY
                rockfordCol = col
                rockfordRow = row
                ADDSCANAROUNDROCKFORD col,row,dir,scanList(),scanListIndex
              ELSE
                room(col+(2*offset),row)=room(col+offset,row)
                room(col+offset,row)=rockfordToDraw
                room(col,row)=EMPTY
                rockfordCol = col+offset
                rockfordRow = row
                ADDSCANAROUNDROCKFORD col,row,dir,scanList(),scanListIndex
              ENDIF
                POSITIONVIEWPORT rockfordCol,rockfordRow
            ENDIF
            MAKESCANCMD rockfordCol,rockfordRow, scanList(), scanListIndex
          ENDIF
        CASE CT_STAND_RCKFD%
          IF (tile AND ASCII%) = ROCKFORD_BASE THEN
            rockfordCol = col
            rockfordRow = row
            MAKESCANCMD rockfordCol,rockfordRow,scanList(),scanListIndex
          ENDIF
      END SELECT
      processedCmds = processedCmds + 1
    ENDIF
    lastCmd=cmdCoords
    rawCmds = rawCmds + 1
  NEXT m

  PAGE WRITE 1
END SUB


SUB MAKEEXPLOSIONCMDS colEpicentre, rowEpicentre, diamondsFlag, cmdList(), cmdIndex
  MAKECMD colEpicentre-1,rowEpicentre-1,CT_EXPLODE%,diamondsFlag,cmdList(),cmdIndex
  MAKECMD colEpicentre  ,rowEpicentre-1,CT_EXPLODE%,diamondsFlag,cmdList(),cmdIndex
  MAKECMD colEpicentre+1,rowEpicentre-1,CT_EXPLODE%,diamondsFlag,cmdList(),cmdIndex

  MAKECMD colEpicentre-1,rowEpicentre  ,CT_EXPLODE%,diamondsFlag,cmdList(),cmdIndex
  MAKECMD colEpicentre  ,rowEpicentre  ,CT_EXPLODE%,diamondsFlag,cmdList(),cmdIndex
  MAKECMD colEpicentre+1,rowEpicentre  ,CT_EXPLODE%,diamondsFlag,cmdList(),cmdIndex

  MAKECMD colEpicentre-1,rowEpicentre+1,CT_EXPLODE%,diamondsFlag,cmdList(),cmdIndex
  MAKECMD colEpicentre  ,rowEpicentre+1,CT_EXPLODE%,diamondsFlag,cmdList(),cmdIndex
  MAKECMD colEpicentre+1,rowEpicentre+1,CT_EXPLODE%,diamondsFlag,cmdList(),cmdIndex
END SUB

'''''''''''''''''''''''''''''''''''
'''''' ROCKFORD SCAN ROUTINE ''''''
'''''''''''''''''''''''''''''''''''
SUB SCANROCKFORD tile, col, row, cmdList(), cmdIndex
  LOCAL rd = keyPress
  LOCAL rc = col
  LOCAL rr = row
  LOCAL ts = TILE_SIZE
  room(rc,rr)=rockfordToDraw
  IF rd = 0 THEN
    MAKECMD rc,rr,CT_STAND_RCKFD%,rd OR firePress,cmdList(),cmdIndex
    EXIT SUB
  ENDIF
  LOCAL INTEGER adjCol = rc
  LOCAL INTEGER adjRow = rr
  DIR2OFFSETS rd,adjCol,adjRow
  LOCAL tileAdj = room(adjCol,adjRow)
 
  IF rd = UP% THEN
    IF (tileAdj AND 255) = EMPTY_BASE THEN
      IF room(rc,adjRow-1) AND HEAVY% THEN
        MAKECMD rc,rr,CT_STAND_RCKFD%,rd OR firePress,cmdList(),cmdIndex
        EXIT SUB
      ENDIF
    ENDIF
  ENDIF

  IF GETEDIBLE(tileAdj) <> 0 THEN
    MAKECMD rc,rr,CT_MOVE_RCKFD%,rd OR firePress,cmdList(),cmdIndex
  ELSEIF (tileAdj AND ASCII%) = ROCK_BASE THEN
    LOCAL adjOffset = 0
    IF rd=LEFT% THEN adjOffset=-1
    IF rd=RIGHT% THEN adjOffset=1
    IF (adjOffset <> 0) AND ((room(adjCol+adjOffset,adjRow) AND ASCII%) = EMPTY_BASE) AND (RND > 0.75) THEN
      MAKECMD rc,rr,CT_PUSH_RCKFD%,rd OR firePress,cmdList(),cmdIndex
    ELSE
      MAKECMD rc,rr,CT_STAND_RCKFD%,rd OR firePress,cmdList(),cmdIndex
    ENDIF
  ELSE
    MAKECMD rc,rr,CT_STAND_RCKFD%,rd OR firePress,cmdList(),cmdIndex
  ENDIF
END SUB



SUB DIR2OFFSETS dir, col, row
  IF dir = LEFT% THEN 
    col=col-1
  ELSEIF dir = RIGHT% THEN
    col=col+1
  ELSEIF dir = UP% THEN
    row=row-1
  ELSEIF dir = DOWN% THEN
    row=row+1
  ENDIF
END SUB

SUB ADDSCANAROUNDROCKFORD col, row, dir, scanList(), scanListIndex
  IF dir = LEFT% THEN 
    scanList(scanListIndex)=(col-2<<36)OR(row-1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col-1<<36)OR(row-1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col<<36)OR(row-1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col+1<<36)OR(row-1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col-2<<36)OR(row<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col-1<<36)OR(row<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col+1<<36)OR(row<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col-2<<36)OR(row+1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col-1<<36)OR(row+1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col<<36)OR(row+1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col+1<<36)OR(row+1<<46)
    scanListIndex = scanListIndex + 1
  ELSEIF dir = RIGHT% THEN
    scanList(scanListIndex)=(col-1<<36)OR(row-1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col<<36)OR(row-1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col+1<<36)OR(row-1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col+2<<36)OR(row-1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col-1<<36)OR(row<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col+1<<36)OR(row<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col+2<<36)OR(row<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col-1<<36)OR(row+1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col<<36)OR(row+1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col+1<<36)OR(row+1<<46)
    scanListIndex = scanListIndex + 1
    scanList(scanListIndex)=(col+2<<36)OR(row+1<<46)
    scanListIndex = scanListIndex + 1
  ELSEIF dir = UP% THEN
    MAKESCANCMD col-1, row-2, scanList(),scanListIndex
    MAKESCANCMD col,   row-2, scanList(),scanListIndex
    MAKESCANCMD col+1, row-2, scanList(),scanListIndex
    MAKESCANCMD col-1, row-1, scanList(),scanListIndex
    MAKESCANCMD col,   row-1, scanList(),scanListIndex
    MAKESCANCMD col+1, row-1, scanList(),scanListIndex
    MAKESCANCMD col-1, row,   scanList(),scanListIndex
    MAKESCANCMD col+1, row,   scanList(),scanListIndex
    MAKESCANCMD col-1, row+1, scanList(),scanListIndex
    MAKESCANCMD col,   row+1, scanList(),scanListIndex
    MAKESCANCMD col+1, row+1, scanList(),scanListIndex
  ELSEIF dir = DOWN% THEN
    MAKESCANCMD col-1, row-1, scanList(),scanListIndex
    MAKESCANCMD col,   row-1, scanList(),scanListIndex
    MAKESCANCMD col+1, row-1, scanList(),scanListIndex
    MAKESCANCMD col-1, row,   scanList(),scanListIndex
    MAKESCANCMD col+1, row,   scanList(),scanListIndex
    MAKESCANCMD col-1, row+1, scanList(),scanListIndex
    MAKESCANCMD col,   row+1, scanList(),scanListIndex
    MAKESCANCMD col+1, row+1, scanList(),scanListIndex
    MAKESCANCMD col-1, row+2, scanList(),scanListIndex
    MAKESCANCMD col,   row+2, scanList(),scanListIndex
    MAKESCANCMD col+1, row+2, scanList(),scanListIndex
  ENDIF
END SUB

SUB ADDSCANFORHEAVY col, row, dir, scanList(), scanListIndex
  scanList(scanListIndex)=(col<<36)OR(row-1<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col-1<<36)OR(row-1<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col+1<<36)OR(row-1<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col+1<<36)OR(row<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col-1<<36)OR(row<<46)
  scanListIndex = scanListIndex + 1
  IF dir = DOWN% THEN
    scanList(scanListIndex)=(col<<36)OR(row+1<<46)
    scanListIndex = scanListIndex + 1
  ENDIF
END SUB

SUB ADDSCANAROUND1 col, row, scanList(), scanListIndex
  scanList(scanListIndex)=(col-1<<36)OR(row-1<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col<<36)OR(row-1<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col+1<<36)OR(row-1<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col-1<<36)OR(row<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col+1<<36)OR(row<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col-1<<36)OR(row+1<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col<<36)OR(row+1<<46)
  scanListIndex = scanListIndex + 1
  scanList(scanListIndex)=(col+1<<36)OR(row+1<<46)
  scanListIndex = scanListIndex + 1
EXIT SUB
  


'''''''DRAWING TO SCREEN'''''''''''

SUB RENDERSCREEN allowPartial
  STATIC lastAnimClock = 0
  STATIC lastRenderClock = 0
  STATIC maxRenderTime = 0
  STATIC maxTickTime = 0
  TIMER = 0
  LOCAL c = animClock
  LOCAL xGap = targetViewportX - viewportX
  LOCAL yGap = targetViewportY - viewportY
  LOCAL hDisp = MAX(4,ABS(xGap/SLIDE_DIVISOR)+2)
  LOCAL vDisp = MAX(4,ABS(yGap/SLIDE_DIVISOR)+2)
  LOCAL dX = SGN(xGap)*MIN(hDisp,ABS(xGap))
  LOCAL dY = SGN(yGap)*MIN(vDisp,ABS(yGap))
  viewportX = viewportX + dX
  viewportY = viewportY + dY
  LOCAL le = FIX(viewportX / TILE_SIZE)
  LOCAL re = FIX((viewportX + VIEWPORT_W_PX - 1)/TILE_SIZE)
  LOCAL te = FIX(viewportY / TILE_SIZE)
  LOCAL be = FIX((viewportY + VIEWPORT_H_PX - 1)/TILE_SIZE)
  RENDERAREA c,te,be,le,re,allowPartial
  IF gameSemaphore=1 AND allowPartial THEN EXIT SUB
  BOX 0,VIEWPORT_H_PX,VIEWPORT_W_PX+TILE_SIZE,TILE_SIZE,1,RGB(RED),RGB(RED)
  BOX VIEWPORT_W_PX,0,TILE_SIZE,VIEWPORT_H_PX+TILE_SIZE,1,RGB(RED),RGB(RED)
  LOCAL elapsed = TIMER
  LOCAL msg AS STRING = "Game Tick Time: " + STR$(gameTickTime) + " max: "+ STR$(maxTickTime) +" Render Time: " + STR$(elapsed) + " max: " + STR$(maxRenderTime)
  'LOCAL msg AS STRING = "Procd Scans: " + STR$(processedScans) + " raw: " + STR$(rawScans) + " Procd Cmds: " + STR$(processedCmds) + " raw: " + STR$(rawCmds) + "     "
  TEXT 0,VIEWPORT_H_PX, msg, "LT", 7, 1, RGB(WHITE), RGB(RED)
  PAGE COPY 1 TO 0, I
  lastAnimClock = animClock
  lastRenderClock = renderClock
  maxRenderTime = MAX(maxRenderTime,elapsed)
  maxTickTime = MAX(maxTickTime,gameTickTime)
END SUB

SUB RENDERAREA c,te,be,le,re,allowPartial
  LOCAL n,m,t,l = TILE_SIZE,xp = viewportX,yp = viewportY
  FOR m=le TO re
    IF gameSemaphore=1 AND allowPartial THEN EXIT SUB
    FOR n=te TO be
      t=room(m,n) : BLIT (c MOD(t>>36 AND 15))*l,(t>>28 AND 240)-l,m*l-xp,n*l-yp,l,l,t>>28 AND 15
    NEXT
  NEXT
END SUB

SUB POSITIONVIEWPORT adjCol AS INTEGER, adjRow AS INTEGER
  LOCAL ts = TILE_SIZE
  LOCAL nearLeftEdge = adjCol*ts < (viewportX + (ts*LEFT_EDGE_BUFFER))
  LOCAL nearRightEdge = adjCol*ts >= (viewportX + VIEWPORT_W_PX - (ts*RIGHT_EDGE_BUFFER))
  LOCAL nearTopEdge = adjRow*ts < (viewportY + (ts*TOP_EDGE_BUFFER))
  LOCAL nearBottomEdge = adjRow*ts >= (viewportY + VIEWPORT_H_PX - (ts*BOTTOM_EDGE_BUFFER))
  IF slidingViewport THEN
    IF nearLeftEdge OR nearRightEdge THEN MOVEVIEWPORT adjCol,0
    IF nearTopEdge OR nearBottomEdge THEN MOVEVIEWPORT 0,adjRow
  ELSE
    IF nearLeftEdge THEN targetViewportX = MAX(0,viewportX-ts)
    IF nearRightEdge THEN targetViewportX = MIN(viewportX+ts,ROOM_W_PX - VIEWPORT_W_PX)
    IF nearTopEdge THEN targetViewportY = MAX(0,viewportY-ts)
    IF nearBottomEdge THEN targetViewportY = MIN(viewportY+ts, ROOM_H_PX - VIEWPORT_H_PX)
    ENDIF
  ENDIF
END SUB

SUB MOVEVIEWPORT centerColumn AS INTEGER, centerRow AS INTEGER
  IF centerColumn > 0 THEN
    LOCAL edgeX = centerColumn * TILE_SIZE - VIEWPORT_HALF_W_PX + (TILE_SIZE/2)
    targetViewportX = MAX(0,edgeX)
    targetViewportX = MIN(targetViewportX, ROOM_W_PX-VIEWPORT_W_PX)
  ENDIF
 
  IF centerRow > 0 THEN  
    LOCAL edgeY = centerRow * TILE_SIZE - VIEWPORT_HALF_H_PX + (TILE_SIZE/2)
    targetViewportY = MAX(0,edgeY)
    targetViewportY = MIN(targetViewportY, ROOM_H_PX-VIEWPORT_H_PX)
  ENDIF
END SUB


'''''''HANDLING INPUTS''''''''''''

SUB KEYSCAN
  LOCAL keyNum
  keyPress = 0
  LOCAL numKeys = KEYDOWN(0) 
  FOR keyNum = 1 TO numKeys
    PROCESSKEY KEYDOWN(keyNum)
  NEXT
  IF keyPress = 0 THEN rockfordToDraw=ROCKFORD
END SUB


SUB PROCESSKEY k
  IF k = 27 THEN
    GOTO CAVESTART
  ENDIF
  IF k = 32 OR k = ASC("0") THEN
    firePress = FIRE_DOWN%
  ELSEIF k = 128 OR k = ASC("o") THEN 
    IF keyPress = 0 THEN 
      keyPress = UP% 
      IF rockfordCol <> 0 AND rockfordRow <> 0 THEN rockfordToDraw=rockfordTile
    ENDIF
  ELSEIF k = 129 OR k = ASC("k") THEN 
    IF keyPress = 0 THEN 
      keyPress = DOWN% 
      IF rockfordCol <> 0 AND rockfordRow <> 0 THEN rockfordToDraw=rockfordTile
    ENDIF
  ELSEIF k = 130 OR k = ASC("x") THEN 'LEFT
    IF keyPress <> RIGHT% THEN 
      keyPress = LEFT%
      rockfordTile = LROCKFORD
      IF rockfordCol <> 0 AND rockfordRow <> 0 THEN rockfordToDraw=LROCKFORD
    ENDIF
  ELSEIF k = 131 OR k = ASC("c") THEN 'RIGHT
    keyPress = RIGHT%
    rockfordTile = RROCKFORD
    IF rockfordCol <> 0 AND rockfordRow <> 0 THEN rockfordToDraw=RROCKFORD
  ELSE
    keyPress = 0
  ENDIF
END SUB

SUB renderTick
  renderClock = renderClock + 1
END SUB

SUB animTick
  animClock = animClock + 1
END SUB

SUB gameTick
  gameSemaphore = 1
END SUB

SUB caveTick
  caveClock = caveClock - 1
  IF magicWallStatus = 1 THEN magicWallActiveTime = magicWallActiveTime + 1
END SUB
 
SUB RESETLEVEL
  BDF2BOARD ROOM_W,ROOM_H,level(),room()
  rockfordCol=FINDROCKFORDCOL(room(), ROOM_W, ROOM_H)
  rockfordRow=FINDROCKFORDROW(room(), ROOM_W, ROOM_H)
  MOVEVIEWPORT rockfordCol,rockfordRow
  gameClock = 0
  magicWallStatus = 0
  magicWallActiveTime = 0
END SUB

SUB FILLSCANLIST scanList(), idx
  LOCAL INTEGER m,n
  FOR m = 1 TO ROOM_H-2
    FOR n = 1 TO ROOM_W-2
      MAKESCANCMD n,m,scanList(),idx
    NEXT
  NEXT
END SUB


SUB ADDBOGUSSCANS scanList(), idx
  LOCAL INTEGER m,n
  FOR m = 5 TO 9
    FOR n = 6 TO 11
      MAKESCANCMD n,m,scanList(),idx
    NEXT
  NEXT
END SUB

SUB TURNMGWALLON
  LOCAL INTEGER m,n
  FOR m = 1 TO ROOM_H-2
    FOR n = 1 TO ROOM_W-2
      IF (room(n,m) AND 255) = MAGIC_WALL_OFF_BASE THEN room(n,m) = MAGIC_WALL_ON
    NEXT
  NEXT
  magicWallStatus=1
END SUB

SUB TURNMGWALLOFF
  LOCAL INTEGER m,n
  FOR m = 1 TO ROOM_H-2
    FOR n = 1 TO ROOM_W-2
      IF (room(n,m) AND 255) = MAGIC_WALL_ON_BASE THEN room(n,m) = MAGIC_WALL_OFF
    NEXT
  NEXT
  magicWallStatus=-1
END SUB

