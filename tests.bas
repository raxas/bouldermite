#INCLUDE "utils.inc"


SUB ASSERT condition AS INTEGER, message AS STRING
  IF condition = 0 THEN PRINT "Error in test: "; message
END SUB

testTile = SETBDF(0,"R")
testTile = SETMOVING(testTile)
testTile = SETROUNDED(testTile)
testTile = SETDIR(testTile, DIR_DOWNLEFT)


ASSERT GETBDF(testTile) = "R", "getbdf1"
ASSERT GETMOVING(testTile) = MOVING_MASK, "getmoving1" 
ASSERT GETROUNDED(testTile) = ROUNDED_MASK, "getrounded1"

testTile = CLEARMOVING(testTile)

ASSERT GETBDF(testTile) = "R", "getbdf2"
ASSERT GETMOVING(testTile) = 0, "getmoving2"
ASSERT GETROUNDED(testTile) = ROUNDED_MASK, "getrounded2"

testTile = CLEARROUNDED(testTile)

ASSERT GETBDF(testTile) = "R", "getbdf3"
ASSERT GETMOVING(testTile) = 0, "getmoving3"
ASSERT GETROUNDED(testTile) = 0, "getrounded3"
ASSERT GETDIR(testTile) = DIR_DOWNLEFT, "getdir1"

testTile = SETXCOORD(testTile, 623)
testTile = SETYCOORD(testTile, 177)

ASSERT GETBDF(testTile) = "R", "getbdf3"
ASSERT GETMOVING(testTile) = 0, "getmoving3"
ASSERT GETROUNDED(testTile) = 0, "getrounded3"
ASSERT GETDIR(testTile) = DIR_DOWNLEFT, "getdir2"
ASSERT GETXCOORD(testTile) = 623, "getxcoord1"
ASSERT GETYCOORD(testTile) = 177, "getycoord1"

testTile = SETXCOORD(testTile, 44)
testTile = SETYCOORD(testTile, 81)
testTile = SETDIR(testTile, DIR_RIGHT)

ASSERT GETBDF(testTile) = "R", "getbdf4"
ASSERT GETMOVING(testTile) = 0, "getmoving4"
ASSERT GETROUNDED(testTile) = 0, "getrounded4"
ASSERT GETDIR(testTile) = DIR_RIGHT, "getdir3"
ASSERT GETXCOORD(testTile) = 44, "getxcoord2"
ASSERT GETYCOORD(testTile) = 81, "getycoord2"

testTile = SETSPRITEPAGE(testTile,11)
testTile = SETSPRITEROW(testTile,12)
testTile = SETSPRITEFRAMES(testTile,8)

ASSERT GETBDF(testTile) = "R", "getbdf5"
ASSERT GETMOVING(testTile) = 0, "getmoving5"
ASSERT GETROUNDED(testTile) = 0, "getrounded5"
ASSERT GETDIR(testTile) = DIR_RIGHT, "getdir4"
ASSERT GETXCOORD(testTile) = 44, "getxcoord3"
ASSERT GETYCOORD(testTile) = 81, "getycoord3"
ASSERT GETSPRITEPAGE(testTile) = 11, "getspritepage1"
ASSERT GETSPRITEROW(testTile) = 12, "getspriterow1"
ASSERT GETSPRITEFRAMES(testTile) = 8, "getspriteframes1"


PRINT "All tests completed."
